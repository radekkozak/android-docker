FROM frolvlad/alpine-glibc:alpine-3.5
MAINTAINER Radek Kozak <radoslaw.kozak@gmail.com>

RUN apk add --no-cache openjdk8
ENV JAVA8_HOME /usr/lib/jvm/default-jvm
ENV JAVA_HOME $JAVA8_HOME

ENV ANDROID_TARGET_SDK="25" \
    ANDROID_BUILD_TOOLS="25.0.2" \
    ANDROID_SDK_TOOLS="25.2.5"

RUN apk add --no-cache curl ca-certificates openssl openssh bash wget tar unzip git bash

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/tools_r${ANDROID_SDK_TOOLS}-linux.zip && \
    unzip android-sdk.zip -d android-sdk

ENV ANDROID_HOME $PWD/android-sdk
ENV PATH $PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

RUN mkdir -p $ANDROID_HOME/licenses
RUN echo -e "\n8933bad161af4178b1185d1a37fbf41ea5269c55" >> $ANDROID_HOME/licenses/android-sdk-license && \
    echo -e "\n84831b9409646a918e30573bab4c9c91346d8abd" >> $ANDROID_HOME/licenses/android-sdk-preview-license

RUN echo y | android-sdk/tools/android --silent update sdk --no-ui --all --filter android-${ANDROID_TARGET_SDK} && \
    echo y | android-sdk/tools/android --silent update sdk --no-ui --all --filter platform-tools && \
    echo y | android-sdk/tools/android --silent update sdk --no-ui --all --filter build-tools-${ANDROID_BUILD_TOOLS}

RUN echo y | android-sdk/tools/android --silent update sdk --no-ui --all --filter extra-android-m2repository && \
    echo y | android-sdk/tools/android --silent update sdk --no-ui --all --filter extra-google-google_play_services && \
    echo y | android-sdk/tools/android --silent update sdk --no-ui --all --filter extra-google-m2repository
